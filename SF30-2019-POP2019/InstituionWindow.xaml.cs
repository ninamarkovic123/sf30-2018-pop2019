﻿using SF30_2019_POP2019.Models;
using SF30_2019_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF30_2019_POP2019
{
    /// <summary>
    /// Interaction logic for InstituionWindow.xaml
    /// </summary>
    public partial class InstituionWindow : Window
    {
        public enum Status { ADD, EDIT }
        public Status _status;
        Institution institution;
        private Institution selectedInstitution;

        public InstituionWindow(Institution institution, Status status = Status.ADD)
        {
            InitializeComponent();
            this.selectedInstitution = institution;
            
            if (institution.Name.Equals(""))
            {
                _status = Status.ADD;
            }
            else
            {
                _status = Status.EDIT;

            }
            this.DataContext = institution;
        }
       


private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if (_status.Equals(Status.ADD))
            {
                if (ProveraInstitution(txtName.Text))
                {
                    MessageBox.Show($"Institution with name{txtName.Text} already exist!", "Error", MessageBoxButton.OK);
                    return;
                }
                else
                {

                    Data.Institutions.Add(selectedInstitution);
                    selectedInstitution.SaveInstitution();
                }
                if (_status.Equals(Status.EDIT))
                    selectedInstitution.UpdateInstitution();
            }
            this.DialogResult = true;
            this.Close();
        }




        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
             this.Close();
    }

        private bool ProveraInstitution(string name)
        {
            foreach (Institution institution in Data.Institutions)
            {
                if (institution.Name.Equals(name))
                {
                    return true;
                }
            }
            return false;
        }
        private Institution GetInstitution(string name)
        {
            foreach (Institution institution in Data.Institutions)
            {
                if (institution.Name.Equals(name))
                {
                    return institution;
                }
            }
            return null;
        }
    }
}

