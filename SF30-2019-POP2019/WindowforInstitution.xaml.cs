﻿using SF30_2019_POP2019.Models;
using SF30_2019_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF30_2019_POP2019
{
    /// <summary>
    /// Interaction logic for WindowforInstitution.xaml
    /// </summary>
    public partial class WindowforInstitution : Window
    {
        ICollectionView view;
        String selectedFilter = "ACTIVE";
        public enum Status { ADD, EDIT, DELETE }


        public WindowforInstitution()
        {
            
            InitializeComponent();
            Data.ReadInstitution();
            InitializeView();
            

        }
        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.Institutions);
            dataInstitutions.ItemsSource = view;
        }
        private void Add_Click(object sender, RoutedEventArgs e)
        {
            Institution institution = new Institution();
            InstituionWindow instituionWindow = new InstituionWindow(institution);
         

            if (instituionWindow.ShowDialog() == true)
            {
                selectedFilter = "ACTIVE";
                view.Refresh();
            }

        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            Institution institution = dataInstitutions.SelectedItem as Institution;
            if (institution == null)
            {
                MessageBox.Show("Warning institution not selected!", "Warning", MessageBoxButton.OK);
            }
            else
            {
                Institution oldInstitution = institution.Clone();
                InstituionWindow instituionWindow = new InstituionWindow(institution);

                if (!(bool)instituionWindow.ShowDialog() == true)

                {
                    int indexOfUser = Data.Institutions.FindIndex(u => u.Name.Equals(oldInstitution.Name));
                    Data.Institutions[indexOfUser] = oldInstitution;
                }
                selectedFilter = "ACTIVE";
                view.Refresh();
            }


        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            Institution institution = dataInstitutions.SelectedItem as Institution;
            if (institution == null)
            {
                MessageBox.Show("Warning institution not selected!", "Warning", MessageBoxButton.OK);
            }
            else
            {
                Institution institutionDelete = GetInstitution(institution.Name);
                institutionDelete.Active = false;
                institutionDelete.DeleteInstitution();

                selectedFilter = "ACTIVE";
                view.Refresh();
            }

        }

        private Institution GetInstitution(string name)
        {
            foreach (Institution institution in Data.Institutions)
            {
                if (institution.Name.Equals(name))
                {
                    return institution;
                }
            }
            return null;
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            Data.GetByNInst(txtName.Text);
            view.Refresh();
        }
        private void SearchLocation_Click(object sender, RoutedEventArgs e)
        {
            Data.GetByLocation(txtLocation.Text);
            view.Refresh();
        }





    }
    }
