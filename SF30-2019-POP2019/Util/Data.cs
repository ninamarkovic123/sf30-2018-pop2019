﻿using SF30_2019_POP2019.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF30_2019_POP2019.Util
{
    class Data
    {
        public static List<User> Users { get; set; }
        
        public static String CONNECTION_STRING = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=70;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        //public static String CONNECTION_STRING = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        
        public static void AddUsers()
        {

            Users = new List<User>();
            /*Administrator admin = new Administrator("pera", "pera");
            Profesor profesor = new Profesor("zika", "zika");
            TeacherAssistent teacherAssistant = new TeacherAssistent("mika", "mika", profesor);
            Administrator admin = new Administrator("Lara", "lara", "lara@gmail.com", "lara", "Laric");
            Profesor profesor = new Profesor("Zika", "zika", "zika@gmail.com", "zika", "Zikic");
            TeacherAssistent teacherAssistant = new TeacherAssistent("Mika", "mika", profesor, "mika@gmail.com", "mika", "Mikic");
            profesor.Assistants.Add(teacherAssistant);

            Users.Add(admin);
            Users.Add(profesor);
            Users.Add(teacherAssistant);*/
        }
        public static List<Clasroom> Classrooms { get; set; }
        public static void AddClassrooms()
        {
            Classrooms = new List<Clasroom>();
            /* Institution institution = new Institution(560, "Fakultet tehnickih nauka", "Bulevar Oslobodjenja 21");
             Clasroom clasroom = new Clasroom(4545, 7, "30", ETypeofClasroom.WITHOUTCOMPUTER, institution);
             Classrooms.Add(clasroom);*/
        }
        public static List<Institution> Institutions { get; set; }
        public static void AddInstitutions()
        {
            Institutions = new List<Institution>();
            /* Institution institution = new Institution(560, "Fakultet tehnickih nauka", "Bulevar Oslobodjenja 21");
             Institutions.Add(institution);*/
        }
        public static List<Appointment> Appointments { get; set; }
        public static void AddAppointments()
        {
            Appointments = new List<Appointment>();
        }
        public static void ReadUser()
        {
            Users = new List<User>();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from dbo.Users";

                da.SelectCommand = command;

                da.Fill(ds, "Users");

                foreach (DataRow row in ds.Tables["Users"].Rows)
                {

                    int id = (int)row["Id"];
                    string name = (string)row["Name"];
                    string lastname = (string)row["Lastname"];
                    string typeOfUser = (string)row["TypeOfUser"];

                    string username = (string)row["Username"];
                    string password = (string)row["Password"];
                    string email = (string)row["Email "];
                    string active = (string)row["Active"];



                    if (row["TypeOfUser"].Equals(ETypeofUser.ADMINISTRATOR.ToString()))
                    {
                        Administrator admin = new Administrator(id,name, lastname, username, password, email);
                        Users.Add(admin);
                    }

                }
            }
        }
        public static void ReadAppointment()
        {
            Appointments = new List<Appointment>();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from dbo.Appointments";

                da.SelectCommand = command;

                da.Fill(ds, "Appointments");

                foreach (DataRow row in ds.Tables["Appointments"].Rows)
                {

                    int id = (int)row["Id"];
                    string time = (string)row["Time"];
                    string day= (string)row["Day"];
                    string typeOfClasroom = (string)row["TypeOfClasroom"];
                    string userAppointment = (string)row["UserAppointment"];
                    string active = (string)row["Active"];



                   
                        Appointment appointment = new Appointment(id,time,userAppointment);
                        Appointments.Add(appointment);
                   

                }
            }
        }
        public static List<Appointment> AppointmentLogin { get; set; }
        public static void ReadAppointmentLogin(string userAppointment)
        {
            AppointmentLogin = new List<Appointment>();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from dbo.Appointments where userAppointment=@UserAppointment";
                command.Parameters.Add(new SqlParameter("userAppointment", "%" + userAppointment + "%"));
                da.SelectCommand = command;

                da.Fill(ds, "Appointments");

                foreach (DataRow row in ds.Tables["Appointments"].Rows)
                {

                    int id = (int)row["Id"];
                    string time = (string)row["Time"];
                    string day = (string)row["Day"];
                    string typeOfClasroom = (string)row["TypeOfClasroom"];
                    string user_Appointment = (string)row["UserAppointment"];
                    string active = (string)row["Active"];




                    Appointment appointment = new Appointment(id, time, userAppointment);
                    AppointmentLogin.Add(appointment);


                }
            }
        }
        public static void ReadTAS()
            {
                Users = new List<User>();
                using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
                {
                    conn.Open();

                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter();

                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"select * from dbo.Users";

                    da.SelectCommand = command;

                    da.Fill(ds, "Users");

                    foreach (DataRow row in ds.Tables["Users"].Rows)
                    {

                        int id = (int)row["Id"];
                        string name = (string)row["Name"];
                        string lastname = (string)row["Lastname"];
                        string typeOfUser = (string)row["TypeOfUser"];

                        string username = (string)row["Username"];
                        string password = (string)row["Password"];
                        string email = (string)row["Email "];
                        string active = (string)row["Active"];



                        if (row["TypeOfUser"].Equals(ETypeofUser.PROFESOR.ToString()))
                        {
                            Profesor profesor = new Profesor(id,name, lastname, username, password, email);
                            Users.Add(profesor);
                        }

                    }
                }


                /*SqlDataReader reader = command.ExecuteReader();
                while(reader.Read())
                {
                    if (reader.GetString(3).Equals(ETypeofUser.ADMINISTRATOR.ToString()))
                    {
                        bool active = (bool)reader.GetValue(4);
                        Users.Add(new Administrator(reader.GetString(2), reader.GetString(1), reader.GetString(5)));
                    }
                */
            }

        public static void GetByName(string name)

        {
            Users.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {

                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from users where name like @name";
                command.Parameters.Add(new SqlParameter("name", "%" + name + "%"));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;

                da.Fill(ds, "Users");
                foreach (DataRow row in ds.Tables["Users"].Rows)
                {
                    int id = (int)row["Id"];
                    string na_me = (string)row["Name"];
                    string lastname = (string)row["Lastname"];
                    string typeOfUser = (string)row["TypeOfUser"];
                    string user_name = (string)row["Username"];
                    string password = (string)row["Password"];
                    string email = (string)row["Email "];
                    string active = (string)row["Active"];


                    if (row["TypeOfUser"].Equals(ETypeofUser.ADMINISTRATOR.ToString()))
                    {
                        Administrator admin = new Administrator(id,name, lastname, user_name, password, email);
                        Users.Add(admin);
                    }



                }
            }


        }
        public static void GetBy(string name)

        {
            Users.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {

                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from users where name like @name";
                command.Parameters.Add(new SqlParameter("name", "%" + name + "%"));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;

                da.Fill(ds, "Users");
                foreach (DataRow row in ds.Tables["Users"].Rows)
                {
                    int id = (int)row["Id"];
                    string na_me = (string)row["Name"];
                    string lastname = (string)row["Lastname"];
                    string typeOfUser = (string)row["TypeOfUser"];
                    string user_name = (string)row["Username"];
                    string password = (string)row["Password"];
                    string email = (string)row["Email "];
                    string active = (string)row["Active"];

                    if (row["TypeOfUser"].Equals(ETypeofUser.ADMINISTRATOR.ToString()))
                    {
                        Administrator admin = new Administrator(id,name, lastname, user_name, password, email);
                        Users.Add(admin);
                    }


                }
            }


        }
        public static void GetByEmail(string email)

        {
            Users.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {

                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from users where email like @email";
                command.Parameters.Add(new SqlParameter("email", "%" + email + "%"));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;

                da.Fill(ds, "Users");
                foreach (DataRow row in ds.Tables["Users"].Rows)
                {
                    int id = (int)row["Id"];
                    string na_me = (string)row["Name"];
                    string lastname = (string)row["Lastname"];
                    string typeOfUser = (string)row["TypeOfUser"];
                    string user_name = (string)row["Username"];
                    string password = (string)row["Password"];
                    string ema_il = (string)row["Email "];
                    string active = (string)row["Active"];

                    if (row["TypeOfUser"].Equals(ETypeofUser.ADMINISTRATOR.ToString()))
                    {
                        Administrator admin = new Administrator(id,na_me, lastname, user_name, password, email);
                        Users.Add(admin);
                    }


                }
            }


        }




        public static void GetByUsername(string username)

        {
            Users.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {

                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from users where username like @username";
                command.Parameters.Add(new SqlParameter("username", "%" + username + "%"));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;

                da.Fill(ds, "Users");
                foreach (DataRow row in ds.Tables["Users"].Rows)
                {
                    int id = (int)row["Id"];
                    string name = (string)row["Name"];
                    string lastname = (string)row["Lastname"];
                    string typeOfUser = (string)row["TypeOfUser"];
                    string user_name = (string)row["Username"];
                    string password = (string)row["Password"];
                    string email = (string)row["Email "];
                    string active = (string)row["Active"];

                    if (row["TypeOfUser"].Equals(ETypeofUser.ADMINISTRATOR.ToString()))
                    {
                        Administrator admin = new Administrator(id,name, lastname, username, password, email);
                        Users.Add(admin);
                    }


                }
            }


        }
        public static void GetByLastname(string lastname)

        {
            Users.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {

                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from users where lastname like @lastname";
                command.Parameters.Add(new SqlParameter("lastname", "%" + lastname + "%"));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;

                da.Fill(ds, "Users");
                foreach (DataRow row in ds.Tables["Users"].Rows)
                {
                    int id = (int)row["Id"];
                    string name = (string)row["Name"];
                    string last_name = (string)row["Lastname"];
                    string typeOfUser = (string)row["TypeOfUser"];
                    string user_name = (string)row["Username"];
                    string password = (string)row["Password"];
                    string email = (string)row["Email "];
                    string active = (string)row["Active"];

                    if (row["TypeOfUser"].Equals(ETypeofUser.ADMINISTRATOR.ToString()))
                    {
                        Administrator admin = new Administrator(id,name, lastname, user_name, password, email);
                        Users.Add(admin);
                    }


                }
            }


        }
        public static void GetByNInst(string name)

        {
            Institutions.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {

                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from institutions where name like @name";
                command.Parameters.Add(new SqlParameter("name", "%" + name + "%"));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;

                da.Fill(ds, "Institutions");
                foreach (DataRow row in ds.Tables["Institutions"].Rows)
                {
                    int id = (int)row["Id"];
                    string na_me = (string)row["Name"];
                    string location = (string)row["Location"];
                    string active = (string)row["Active"];



                    Institution institution = new Institution(id, name, location);
                    Institutions.Add(institution);


                }
            }


        }
        public static void GetById(int id)

        {
            Institutions.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {

                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from institutions where id like @id";
                command.Parameters.Add(new SqlParameter("id", "%" + id + "%"));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;

                da.Fill(ds, "Institutions");
                foreach (DataRow row in ds.Tables["Institutions"].Rows)
                {
                    int i_d = (int)row["Id"];
                    string name = (string)row["Name"];
                    string location = (string)row["Location"];
                    string active = (string)row["Active"];



                    Institution institution = new Institution(id, name, location);
                    Institutions.Add(institution);


                }
            }


        }
        public static void GetByLocation(string location)

        {
            Institutions.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {

                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from institutions where location like @location";
                command.Parameters.Add(new SqlParameter("location", "%" + location + "%"));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;

                da.Fill(ds, "Institutions");
                foreach (DataRow row in ds.Tables["Institutions"].Rows)
                {
                    int id = (int)row["Id"];
                    string name = (string)row["Name"];
                    string loca_tion = (string)row["Location"];
                    string active = (string)row["Active"];



                    Institution institution = new Institution(id, name, location);
                    Institutions.Add(institution);


                }
            }


        }
        public static void GetBySeats(string seats)

        {
            Classrooms.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {

                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from classrooms where seats like @seats";
                command.Parameters.Add(new SqlParameter("seats", "%" + seats + "%"));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;

                da.Fill(ds, "Classrooms");
                foreach (DataRow row in ds.Tables["Classrooms"].Rows)
                {
                    int id = (int)row["Id"];
                    int number = (int)row["Number"];
                    string sea_ts = (string)row["Seats"];
                    string type = (string)row["Type"];
                    ETypeofClasroom eTypeofClasroom = (ETypeofClasroom)Enum.Parse(typeof(ETypeofClasroom), type);
                    string institution = (string)row["Institution"];

                    string active = (string)row["Active"];



                    Clasroom clasroom = new Clasroom(id, number, seats, institution);
                    Classrooms.Add(clasroom);


                }
            }


        }
        public static void GetByInstitution(string institution)

        {
            Classrooms.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {

                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from classrooms where institution like @institution";
                command.Parameters.Add(new SqlParameter("institution", "%" + institution + "%"));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;

                da.Fill(ds, "Classrooms");
                foreach (DataRow row in ds.Tables["Classrooms"].Rows)
                {
                    int id = (int)row["Id"];
                    int number = (int)row["Number"];
                    string seats = (string)row["Seats"];
                    string type = (string)row["Type"];
                    ETypeofClasroom eTypeofClasroom = (ETypeofClasroom)Enum.Parse(typeof(ETypeofClasroom), type);
                    string ins_titution = (string)row["Institution"];

                    string active = (string)row["Active"];



                    Clasroom clasroom = new Clasroom(id, number, seats, institution);
                    Classrooms.Add(clasroom);


                }
            }


        }
        public static void GetByUserApp(string userApp)

        {
            Appointments.Clear();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {

                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from appointments where userAppointment like @UserAppointment";
                command.Parameters.Add(new SqlParameter("userAppointment", "%" + userApp + "%"));
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;

                da.Fill(ds, "Appointments");
                foreach (DataRow row in ds.Tables["Appointments"].Rows)
                {
                    int id = (int)row["Id"];
                    string time = (string)row["Time"];
                    string day = (string)row["Day"];
                    string typeOfClasroom = (string)row["TypeOfClasroom"];
                    string userAppointment = (string)row["UserAppointment"];
                    string active = (string)row["Active"];




                    Appointment appointment = new Appointment(id, time, userAppointment);
                    Appointments.Add(appointment);


                }
            }


        }
        public static void ReadInstitution()
        {
            Institutions = new List<Institution>();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from dbo.Institutions";

                da.SelectCommand = command;

                da.Fill(ds, "Institutions");

                foreach (DataRow row in ds.Tables["Institutions"].Rows)
                {
                    int id = (int)row["Id"];
                    string name = (string)row["Name"];
                    string location = (string)row["Location"];
                    string active = (string)row["Active"];



                    Institution institution = new Institution(id, name, location);
                    Institutions.Add(institution);
                }

            }
        }
        public static void ReadClassroom()
        {
            Classrooms = new List<Clasroom>();
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from dbo.Classrooms";

                da.SelectCommand = command;

                da.Fill(ds, "Classrooms");

                foreach (DataRow row in ds.Tables["Classrooms"].Rows)
                {

                    int id = (int)row["Id"];
                    int number = (int)row["Number"];
                    string seats = (string)row["Seats"];
                    string type = (string)row["Type"];
                    ETypeofClasroom eTypeofClasroom = (ETypeofClasroom)Enum.Parse(typeof(ETypeofClasroom), type);
                    string institution = (string)row["Institution"];

                    string active = (string)row["Active"];



                    Clasroom clasroom = new Clasroom(id, number, seats,institution);
                    Classrooms.Add(clasroom);
                }

            }
        }
        public static void ReadProfessor()
        {
            Users = new List<User>();
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from dbo.Users";

                da.SelectCommand = command;

                da.Fill(ds, "Users");

                foreach (DataRow row in ds.Tables["Users"].Rows)
                {

                    int id = (int)row["Id"];
                    string name = (string)row["Name"];
                    string lastname = (string)row["Lastname"];
                    string typeOfUser = (string)row["TypeOfUser"];

                    string username = (string)row["Username"];
                    string password = (string)row["Password"];
                    string email = (string)row["Email "];
                    string active = (string)row["Active"];



                    if (row["TypeOfUser"].Equals(ETypeofUser.PROFESOR.ToString()))
                    {
                        Profesor profesor = new Profesor(id,name, lastname, username, password, email);
                        Users.Add(profesor);
                    }
                }
            }
        }
        public static void ReadTa()
        {
            Users = new List<User>();
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();

                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select * from dbo.Users";

                da.SelectCommand = command;

                da.Fill(ds, "Users");

                foreach (DataRow row in ds.Tables["Users"].Rows)
                {

                    int id = (int)row["Id"];
                    string name = (string)row["Name"];
                    string lastname = (string)row["Lastname"];
                    string typeOfUser = (string)row["TypeOfUser"];

                    string username = (string)row["Username"];
                    string password = (string)row["Password"];
                    string email = (string)row["Email "];
                    string active = (string)row["Active"];



                    if (row["TypeOfUser"].Equals(ETypeofUser.TA.ToString()))
                    {
                        TeacherAssistent teacherAssistent = new TeacherAssistent(id,name, username, lastname, password, email);
                        Users.Add(teacherAssistent);
                    }
                }
            }
        }
    }
}
            
    



    
