﻿using SF30_2019_POP2019.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF30_2019_POP2019
{
    /// <summary>
    /// Interaction logic for AppointForTP.xaml
    /// </summary>
    public partial class AppointForTP : Window
    {
        ICollectionView view;
        public AppointForTP()
        {
            InitializeComponent();
            Data.ReadAppointmentLogin(Login.loginUsername);
            InitializeView();
        }
        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.AppointmentLogin);
            data.ItemsSource = view;
            data.IsSynchronizedWithCurrentItem = true;
        }
    }
}
