﻿using SF30_2019_POP2019.Models;
using SF30_2019_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF30_2019_POP2019
{
    /// <summary>
    /// Interaction logic for WindowforClassroom.xaml
    /// </summary>
    public partial class WindowforClassroom : Window
    {
        public enum Status { ADD, EDIT, DELETE }
        ICollectionView view;
        private Status status;
        String selectedFilter = "ACTIVE";
        public WindowforClassroom()
        {
            InitializeComponent();
            Data.ReadClassroom();
            InitializeView();



        }
        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.Classrooms);
            dataClassrooms.ItemsSource = view;
        }


        private void Add_Click(object sender, RoutedEventArgs e)
        {
            Clasroom clasroom = new Clasroom();
            AddClassrom addClassrom = new AddClassrom(clasroom);


            if (addClassrom.ShowDialog() == true)
            {
                selectedFilter = "ACTIVE";
                view.Refresh();
            }

        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            Clasroom clasroom = dataClassrooms.SelectedItem as Clasroom;
            if (clasroom == null)
            {
                MessageBox.Show("Warning classroom not selected!", "Warning", MessageBoxButton.OK);
            }
            else
            {
                Clasroom oldClassroom = clasroom.Clone();
                AddClassrom addClassrom = new AddClassrom(clasroom);

                if (!(bool)addClassrom.ShowDialog() == true)

                {
                    int indexOfUser = Data.Classrooms.FindIndex(u => u.Seats.Equals(oldClassroom.Seats));
                    Data.Classrooms[indexOfUser] = oldClassroom;
                }
                selectedFilter = "ACTIVE";
                view.Refresh();
            }
        }
        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            Clasroom clasroom = dataClassrooms.SelectedItem as Clasroom;
            if (clasroom == null)
            {
                MessageBox.Show("Warning classroom not selected!", "Warning", MessageBoxButton.OK);
            }
            else
            {
                Clasroom classroomDelete = GetClassroom(clasroom.Seats);
                classroomDelete.Active = false;
                classroomDelete.DeleteClassroom();

                selectedFilter = "ACTIVE";
                view.Refresh();
            }
        }




        private Clasroom GetClassroom(string seats)
        {
            foreach (Clasroom clasroom in Data.Classrooms)
            {
                if (clasroom.Seats.Equals(seats))
                {
                    return clasroom;
                }
            }
            return null;
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            Data.GetBySeats(txtSeats.Text);
            view.Refresh();
        }
        private void SearchInstitution_Click(object sender, RoutedEventArgs e)
        {
            Data.GetByInstitution(txtInstitution.Text);
            view.Refresh();
        }

    }
}


