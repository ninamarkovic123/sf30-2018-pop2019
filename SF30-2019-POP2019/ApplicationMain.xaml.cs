﻿using SF30_2019_POP2019.Models;
using SF30_2019_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF30_2019_POP2019
{
    /// <summary>
    /// Interaction logic for ApplicationMain.xaml
    /// </summary>
    public partial class ApplicationMain : Window
    {
        ICollectionView view;
        public ApplicationMain()
        {
            
            Data.AddUsers();
            Data.AddClassrooms();
            Data.AddInstitutions();
            InitializeComponent();
            InitializeView();
        }
        private void InitializeView()
        {
            // view = CollectionViewSource.GetDefaultView(Data.Users);
            //  dataUsers.ItemsSource = view;
        }

        private void Classrooms_Click(object sender, RoutedEventArgs e)
        {
            User user = new User();
            WindowforClassroom classroomWindow = new WindowforClassroom();
            classroomWindow.ShowDialog();

        }

        private void Institutions_Click(object sender, RoutedEventArgs e)
        {
            User user = new User();
            WindowforInstitution institutionWindow = new WindowforInstitution();
            institutionWindow.ShowDialog();
        }

        private void Users_Click(object sender, RoutedEventArgs e)
        {
            User user = new User();
            MainWindow mainWindow = new MainWindow();
            mainWindow.ShowDialog();

        }

        private void btnAppointment_Click(object sender, RoutedEventArgs e)
        {
            User user = new User();
            WindowforAppointment windowforAppointment = new WindowforAppointment();
            windowforAppointment.ShowDialog();
        }
    }
}
