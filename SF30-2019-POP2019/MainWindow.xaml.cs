﻿using SF30_2019_POP2019.Models;
using SF30_2019_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;

using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SF30_2019_POP2019
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ICollectionView view;
        String selectedFilter = "ACTIVE";
        public MainWindow()
        {
            InitializeComponent();
            Data.ReadUser();
            InitializeView();
           

            view.Filter = CustomFilter;
        }

        private bool CustomFilter(object obj)
        {
            User user = obj as User;
            if (selectedFilter.Equals("SEARCH_USERNAME"))
                return user.Active && user.UserName.Contains(txtSearchUsername.Text);
            return user.Active;
          
        }
       

            private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.Users);
            dataUsers.ItemsSource = view;
            dataUsers.IsSynchronizedWithCurrentItem = true;
            /* List<User> users = new List<User>();
             foreach (User user in Data.Users)
             {
                 if (user.Active)
                 {
                     users.Add(user);
                 }
             }

             dataUsers.ItemsSource = users;
            //dataUsers.ItemsSource = Data.Users.FindAll(u => u.Active); Pronalazi aktivne user-e
         */

        }
     /*   private void LoadTable()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from users";



                command.ExecuteNonQuery();
                SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                DataTable dt = new DataTable("Users");
                dataAdapter.Fill(dt);
                dataUsers.ItemsSource = dt.DefaultView;
                dataAdapter.Update(dt);
            }
        }
        */

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            Administrator administrator = new Administrator();
            UserWindow userWindow = new UserWindow(administrator);

            if (userWindow.ShowDialog() == true)
            {
                selectedFilter = "ACTIVE";
                view.Refresh();
            }


        }


        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            User user = dataUsers.SelectedItem as User;
            if (user == null)
            {
                MessageBox.Show("Warning user not selected!", "Warning", MessageBoxButton.OK);
            }
            else
            {
                User oldUser = user.Clone();
                UserWindow userWindow = new UserWindow(user);

                if (!(bool)userWindow.ShowDialog() == true)

                {
                    int indexOfUser = Data.Users.FindIndex(u => u.UserName.Equals(oldUser.UserName));
                    Data.Users[indexOfUser] = oldUser;
                }
                selectedFilter = "ACTIVE";
                view.Refresh();
            }

        }
        private void Name_Click(object sender, RoutedEventArgs e)
        {
            

            Data.GetByName(txtSearchName.Text);
            view.Refresh();

        }
        private void Lastname_Click(object sender, RoutedEventArgs e)
        {


            Data.GetByLastname(txtSearchLastname.Text);
            view.Refresh();

        }
        private void Email_Click(object sender, RoutedEventArgs e)
        {


            Data.GetByEmail(txtSearchEmail.Text);
            view.Refresh();

        }



        private void Button_Click(object sender, RoutedEventArgs e)
        {
            /*selectedFilter = "SEARCH_USERNAME";*/

            Data.GetByUsername(txtSearchUsername.Text);
            view.Refresh();

        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            User user = dataUsers.SelectedItem as User;
            if (user == null)
            {
                MessageBox.Show("Warning user not selected!", "Warning", MessageBoxButton.OK);
            }
            else
            {
                User userDelete = GetUser(user.UserName);
                userDelete.Active = false;
                userDelete.DeleteUser();

                selectedFilter = "ACTIVE";
                view.Refresh();
            }
        }


        private User GetUser(string userName)
        {
            foreach (User user in Data.Users)
            {
                if (user.UserName.Equals(userName))
                {
                    return user;
                }
            }
            return null;
        }

        private void dataUsers_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Error") || e.PropertyName.Equals("Active"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
        }
      


        }
   

    }

