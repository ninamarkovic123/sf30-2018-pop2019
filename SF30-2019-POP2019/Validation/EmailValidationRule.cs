﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SF30_2019_POP2019.Validation
{
    class EmailValidationRule : ValidationRule
    {
        Regex regex = new Regex(@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
+ "@"
+ @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$");
        //Regex regex = new Regex(@"b[a-z0-9]@[a-z0-9]{1,10}\.[a-z]{2,3}\b", RegexOptions.IgnoreCase);
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string v = value as string;
            if (v != null && !v.Equals(string.Empty) && regex.Match(v).Success) 
                return new ValidationResult(true, null);
            if (v.Equals(string.Empty))
                 return new ValidationResult(false, "This field is required");
            
            return new ValidationResult(false, "wrong format");
        }
    }
}
