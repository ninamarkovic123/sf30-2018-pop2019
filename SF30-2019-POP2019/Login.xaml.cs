﻿using SF30_2019_POP2019.Models;
using SF30_2019_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF30_2019_POP2019
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {

        public static string loginUsername;
        public static String CONNECTION_STRING = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=70;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        public Login()
        {
            InitializeComponent();
        }
        public void btnPrijava_Click(object sender, RoutedEventArgs e)
        {
            string query = "SELECT TypeOfUser from users WHERE Username = @username and password=@password";
            string returnValue = "";
            loginUsername = txtUsername.Text;
            using (SqlConnection con = new SqlConnection(CONNECTION_STRING))
            {
                using (SqlCommand sqlcmd = new SqlCommand(query, con))
                {
                    sqlcmd.Parameters.Add("@username", SqlDbType.VarChar).Value = txtUsername.Text;
                    sqlcmd.Parameters.Add("@password", SqlDbType.VarChar).Value = txtPassword.Text;
                    con.Open();
                    returnValue = (string)sqlcmd.ExecuteScalar();
                }
            }
          
            if (String.IsNullOrEmpty(returnValue))
            {
                MessageBox.Show("Incorrect username or password");
                return;
            }
            returnValue = returnValue.Trim();
            if (returnValue == "ADMINISTRATOR")
            {
                MessageBox.Show("You are logged in as an Admin");
                ApplicationMain fr1 = new ApplicationMain();
                fr1.Show();
                this.Hide();
            }
            else if (returnValue == "PROFESOR")
            {
                MessageBox.Show("You are logged in as a Professor");
                Professor fr2 = new Professor();
                fr2.Show();
                this.Hide();
            }
            else if (returnValue == "TA")
            {
                MessageBox.Show("You are logged in as a TA");
                TA fr3 = new TA();
                fr3.Show();
                this.Hide();
            }
        }


        private void btnNastavi_Click(object sender, RoutedEventArgs e)
        {
            WindowForUnregistered fr4 = new WindowForUnregistered();
            fr4.Show();
            this.Hide();
        }


        private void btnClose_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    }

}

      
