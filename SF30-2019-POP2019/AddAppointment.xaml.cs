﻿using SF30_2019_POP2019.Models;
using SF30_2019_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF30_2019_POP2019
{
    /// <summary>
    /// Interaction logic for AddAppointment.xaml
    /// </summary>
    public partial class AddAppointment : Window
    {
        private Status _status;
        private Appointment selectedAppointment;

        public enum Status { ADD, EDIT }

        public AddAppointment(Appointment appointment)
        {
           
            InitializeComponent();
            fillCombo();
            this.selectedAppointment = appointment;
            cmbTypeOfDay.ItemsSource = new List<EDay>(){EDay.Monday,EDay.Tuesday,EDay.Wednesday,EDay.Thursday,EDay.Friday, EDay.Saturday, EDay.Sunday  };
            cmbTypeOfClassroom.ItemsSource = new List<ETypeofClasroom>() { ETypeofClasroom.COMPUTER, ETypeofClasroom.WITHOUTCOMPUTER };

            if (appointment.Time.Equals(""))
            {

                _status = Status.ADD;
            }
            else
            {
                _status = Status.EDIT;
               
            }
            this.DataContext = appointment;
        }

        void fillCombo()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from users";

                SqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    string Name = dr.GetString(1);
                    cmbUser.Items.Add(Name);
                }
            }
        }
                public AddAppointment()
        {

        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if (_status.Equals(Status.ADD))
            {
                if (ProveraAppointment(txtTime.Text))
                {
                    MessageBox.Show($"Appointment with time{txtTime.Text} already exist!", "Error", MessageBoxButton.OK);
                    return;
                }
                else
                {

                    Data.Appointments.Add(selectedAppointment);
                    selectedAppointment.SaveAppointment();
                }
                if (_status.Equals(Status.EDIT))
                    selectedAppointment.UpdateAppointment();
            }
            this.DialogResult = true;
            this.Close();
        }


        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
        private bool ProveraAppointment(string time)
        {
            foreach (Appointment appointment in Data.Appointments)
            {
                if (appointment.Time.Equals(time))
                {
                    return true;
                }
            }
            return false;
        }
        private Appointment GetAppointment(string time)
        {
            foreach (Appointment appointment in Data.Appointments)
            {
                if (appointment.Time.Equals(time))
                {
                    return appointment;
                }
            }
            return null;
        }
    }
    }

