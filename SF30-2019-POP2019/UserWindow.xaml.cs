﻿using SF30_2019_POP2019.Models;
using SF30_2019_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF30_2019_POP2019
{
    /// <summary>
    /// Interaction logic for UserWindow.xaml
    /// </summary>

    public partial class UserWindow : Window
    {
        private Status _status;
        private User selectedUser;

        public enum Status { ADD, EDIT }




        public UserWindow(User user)
        {
            InitializeComponent();
            this.selectedUser = user;
            cmbTypeOfUser.ItemsSource = new List<ETypeofUser>() { ETypeofUser.ADMINISTRATOR, ETypeofUser.PROFESOR, ETypeofUser.TA };
            if (user.UserName.Equals(""))
            {
                
                _status = Status.ADD;
            }
            else
            {
                _status = Status.EDIT;
                /*
                    txtName.Text = user.Name;
                    txtUser.Text = user.UserName;
                    txtUser.IsReadOnly = true;
                    cmbTypeOfUser.SelectedValue = user.TypeOfUser;
                    */
            }
            this.DataContext = user;
        }

        public UserWindow()
        {
        }

        private void txtName_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (_status.Equals(Status.ADD))
            {
                if (ProveraUser(txtUser.Text))
                {
                    MessageBox.Show($"User with username{txtUser.Text} already exist!", "Error", MessageBoxButton.OK);
                    return;
                }
                else
                {

                    Data.Users.Add(selectedUser);
                    selectedUser.SaveUser();
                }
                if (_status.Equals(Status.EDIT))
                    selectedUser.UpdateUser();
            }
            this.DialogResult = true;
            this.Close();
        }
    
       
            private void btnCancel_Click(object sender, RoutedEventArgs e)
            {
                this.DialogResult = false;
                this.Close();
            }
            private bool ProveraUser(string username)
            {
                foreach (User user in Data.Users)
                {
                    if (user.UserName.Equals(username))
                    {
                        return true;
                    }
                }
                return false;
            }
            private User GetUser(string username)
            {
                foreach (User user in Data.Users)
                {
                    if (user.UserName.Equals(username))
                    {
                        return user;
                    }
                }
                return null;
            }

     
    }
    }

    

