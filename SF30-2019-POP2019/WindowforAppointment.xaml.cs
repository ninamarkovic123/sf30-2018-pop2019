﻿using SF30_2019_POP2019.Models;
using SF30_2019_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF30_2019_POP2019
{
    /// <summary>
    /// Interaction logic for WindowforAppointment.xaml
    /// </summary>
    public partial class WindowforAppointment : Window
    {
        ICollectionView view;
        String selectedFilter = "ACTIVE";
        public WindowforAppointment()
        {
            InitializeComponent();
            Data.ReadAppointment();
            InitializeView();



        }
        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.Appointments);
            dataAppointment.ItemsSource = view;
            dataAppointment.IsSynchronizedWithCurrentItem = true;
        }
        private void Add_Click(object sender, RoutedEventArgs e)
        {
            Appointment appointment = new Appointment();
            AddAppointment addAppointment = new AddAppointment(appointment);

            if (addAppointment.ShowDialog() == true)
            {
                selectedFilter = "ACTIVE";
                view.Refresh();
            }
        }
            private void Edit_Click(object sender, RoutedEventArgs e)
            {
                Appointment appointment = dataAppointment.SelectedItem as Appointment;
                if (appointment == null)
                {
                    MessageBox.Show("Warning appointment not selected!", "Warning", MessageBoxButton.OK);
                }
                else
                {
                    Appointment oldAppointment = appointment.Clone();
                    AddAppointment addAppointment = new AddAppointment(appointment);

                    if (!(bool)addAppointment.ShowDialog() == true)

                    {
                        int indexOfUser = Data.Appointments.FindIndex(u => u.Time.Equals(oldAppointment.Time));
                        Data.Appointments[indexOfUser] = oldAppointment;
                    }
                    selectedFilter = "ACTIVE";
                    view.Refresh();
                }

            }
        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            Appointment appointment = dataAppointment.SelectedItem as Appointment;
            if (appointment == null)
            {
                MessageBox.Show("Warning appointment not selected!", "Warning", MessageBoxButton.OK);
            }
            else
            {
                Appointment appointmentDelete = GetAppointment(appointment.Time);
                appointmentDelete.Active = false;
                appointmentDelete.DeleteAppointment();

                selectedFilter = "ACTIVE";
                view.Refresh();
            }
        }


        private Appointment GetAppointment(string time)
        {
            foreach (Appointment appointment in Data.Appointments)
            {
                if (appointment.Time.Equals(time))
                {
                    return appointment;
                }
            }
            return null;
        }

        private void dataUsers_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Error") || e.PropertyName.Equals("Active"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            Data.GetByUserApp(txtUser.Text);
            view.Refresh();

        }
    }
}
