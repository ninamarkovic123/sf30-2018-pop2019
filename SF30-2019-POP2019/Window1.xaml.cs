﻿using SF30_2019_POP2019.Models;
using SF30_2019_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF30_2019_POP2019
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        ICollectionView view;
        public Window1()
        {
            InitializeComponent();
            Data.ReadProfessor();
            InitializeView();
        }
        private void InitializeView()
        {
            view = CollectionViewSource.GetDefaultView(Data.Users);
            dataProfesor.ItemsSource = view;
            dataProfesor.IsSynchronizedWithCurrentItem = true;

        }
    }    }

   


    
