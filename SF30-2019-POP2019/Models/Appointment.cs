﻿using SF30_2019_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF30_2019_POP2019.Models
{
    public class Appointment
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string time;

        public string Time
        {
            get { return time; }
            set { time = value; }
        }
        private EDay day;
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public EDay Day

        {
            get { return day; }
            set { day = value; OnPropertyChanged("Day"); }
        }

        private ETypeofClasroom typeofClasroom;

        public ETypeofClasroom TypeofClasroom
        {
            get { return typeofClasroom; }
            set { typeofClasroom = value; OnPropertyChanged("TypeOfClasroom"); }
        }
        private string userAppointment;

        public  string UserAppointment
        {
            get { return userAppointment; }
            set { userAppointment = value; OnPropertyChanged("UserAppointment"); }
        }
        private bool _active;

        public bool Active
        {
            get { return _active; }
            set { _active = value; OnPropertyChanged("Active"); }
        }

      

        public string this[string columnName] => throw new NotImplementedException();

        public Appointment(int id,string time,string userAppointment)
        {
            this.id = id;
            this.time = time;
            this.userAppointment = userAppointment;
            this._active = true;
        }
        public Appointment()
        {
            this.Time = "";
            this._active = true;
        }
        public virtual Appointment Clone()
        {
            return null;

        }
        public virtual int SaveAppointment()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into appointments (time,day,typeofclasroom,userappointment,active)output inserted.id values (@Time,@Day,@TypeOfClasroom,@UserAppointment,@Active)";

                command.Parameters.Add(new SqlParameter("Time", this.Time));
                command.Parameters.Add(new SqlParameter("Day", this.Day.ToString()));
                command.Parameters.Add(new SqlParameter("TypeOfClasroom", this.TypeofClasroom.ToString()));
                command.Parameters.Add(new SqlParameter("UserAppointment", this.UserAppointment));
           



                command.Parameters.Add(new SqlParameter("Active", this.Active));

                id = (int)command.ExecuteScalar();
            }
            return id;
        }
        public virtual void UpdateAppointment()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update appointments set day=@Day,typeofclasroom=@TypeOfClasroom,userAppointment=@UserAppointment,  where time=@Time";

                command.Parameters.Add(new SqlParameter("Time", this.Time));
                command.Parameters.Add(new SqlParameter("Day", this.Day.ToString()));
                command.Parameters.Add(new SqlParameter("TypeOfClasroom", this.TypeofClasroom.ToString()));
                command.Parameters.Add(new SqlParameter("UserAppointment", this.UserAppointment));
               

                command.ExecuteNonQuery();
            }
        }

        public virtual void DeleteAppointment()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"delete from appointments  where time=@time and active=@active";

                command.Parameters.Add(new SqlParameter("active", this.Active));
                command.Parameters.Add(new SqlParameter("time", this.Time));

                command.ExecuteNonQuery();
            }
        }
        public virtual int SelectedAppointmentId()
        {
            int id = 1;
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select id from appointments where time like @Time";
                command.Parameters.Add(new SqlParameter("Time", this.Time));

                id = (int)command.ExecuteScalar();
            }
            return id;
        }
    }
}
