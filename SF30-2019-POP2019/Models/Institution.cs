﻿using SF30_2019_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF30_2019_POP2019.Models
{
    public class Institution : INotifyPropertyChanged
    {
        private int id;
        private string name;
        private string location;
        private bool _active;


        public Institution(int id, string name, string location)
        {
            this.id = id;
            this._active = true;
            this.name = name;
            this.location = location;
         
        }

        

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Name
        {
            get { return name; }
            set
            {
                name = value; OnPropertyChanged("Name");
            }
        }
        public string Location
        {
            get { return location; }
            set { location = value; OnPropertyChanged("Location"); }
        }

        public bool Active
        {
            get { return _active; }
            set { _active = value; OnPropertyChanged("Active"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public Institution()
        {
            this.Name = "";
            this._active = true;
        }
        public virtual Institution Clone()
        {
            /*Institution institution = new Institution(this.id, this.name, this.location);
            return institution;*/
            return null;
        }
        public virtual int SaveInstitution()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into institutions (name,location,active)output inserted.id values (@Name,@Location,@Active)";

                command.Parameters.Add(new SqlParameter("Name", this.Name));
                command.Parameters.Add(new SqlParameter("Location", this.Location));
                command.Parameters.Add(new SqlParameter("Active", this.Active));

                id = (int)command.ExecuteScalar();
            }
            return id;
        }
        public virtual void UpdateInstitution()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update institutions set location=@location  where name=@name";

                command.Parameters.Add(new SqlParameter("name", this.Name));
                command.Parameters.Add(new SqlParameter("location", this.Location));
                



                command.ExecuteNonQuery();
            }
        }
        public virtual void DeleteInstitution()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"delete from institutions where name=@name and active=@active";

                command.Parameters.Add(new SqlParameter("active", this.Active));
                command.Parameters.Add(new SqlParameter("name", this.Name));

                command.ExecuteNonQuery();
            }
        }
        public virtual int SelectedInstitutionId()
        {
            int id = 1;
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select id from institution where name like @Name";
                command.Parameters.Add(new SqlParameter("Name", this.Name));

                id = (int)command.ExecuteScalar();
            }
            return id;
        }



    }

}
