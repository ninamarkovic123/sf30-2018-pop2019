﻿using SF30_2019_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF30_2019_POP2019.Models
{
    public class Administrator : User
    {
        public Administrator() : base()
        {
            TypeOfUser = ETypeofUser.ADMINISTRATOR;
        }

        public Administrator(int id,string name, string username, string lastname, string email, string password) : base(id,name, username, lastname, email, password)
        {
            TypeOfUser = ETypeofUser.ADMINISTRATOR;
        }

        public override User Clone()
        {
            Administrator administrator = new Administrator(this.Id,this.Name, this.UserName, this.LastName, this.Email, this.Password);
            return administrator;
        }
        public override int SaveUser()
        {
            int id = base.SaveUser();
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into administrators (id) values (@id)";
                command.Parameters.Add(new SqlParameter("id", id));
                command.ExecuteNonQuery();
            }
            return id;
        }
        public override void UpdateUser()
        {
            base.UpdateUser();
        }
        public override int SelectedUserId()
        {
            return base.SelectedUserId();
        }

    }
}
