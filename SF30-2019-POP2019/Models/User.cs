﻿using SF30_2019_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF30_2019_POP2019.Models
{
    public  class User : INotifyPropertyChanged, IDataErrorInfo
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set
            {
                if (value.Equals(string.Empty))
                    throw new ArgumentException("This field is required.");
                _name = value; OnPropertyChanged("Name"); }
        }
        private string _lastName;

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        private ETypeofUser _typeOfUser;

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public ETypeofUser TypeOfUser
        {
            get { return _typeOfUser; }
            set { _typeOfUser = value; OnPropertyChanged("TypeOfUser"); }
        }


        private string _userName;

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; OnPropertyChanged("Username"); }
        }

        private string _password;

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        private string _email;

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

      
        private bool _active;

        public bool Active
        {
            get { return _active; }
            set { _active = value; OnPropertyChanged("Active"); }
        }

        
        public string Error {
            get { return null; }
        }
        public string this[string columnName]
        {
            get {
                switch (columnName) {
                    
                    case "Name":
                        if (Name != null && Name.Equals(string.Empty))
                        return "Field required!";
                        break;
                }
                return String.Empty;
            }
        }

        public User(int id,string name, string lastname, string username, string password,string email  )
        {
            this.id = id;
            this._name = name;
            this._lastName = lastname;
            this._userName = username;
            this._active = true;
            this._password = password;
            this._email = email;
            
            
            
            
        }

        public User()
        {
            this.UserName = "";
            this._active = true;
          
        }
      
        

        public virtual User Clone()
        {
            return null;
        }

        public virtual int SaveUser()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into users (name,lastname,typeofuser,username,password,email,active)output inserted.id values (@Name,@Lastname,@TypeOfUser,@Username,@Password,@Email,@Active)";

                command.Parameters.Add(new SqlParameter("Name", this.Name));
                command.Parameters.Add(new SqlParameter("Lastname", this.LastName));
                command.Parameters.Add(new SqlParameter("TypeOfUser", this.TypeOfUser.ToString()));
                command.Parameters.Add(new SqlParameter("Username", this.UserName));
                command.Parameters.Add(new SqlParameter("Password", this.Password));
                command.Parameters.Add(new SqlParameter("Email", this.Email));
                
                
                
                command.Parameters.Add(new SqlParameter("Active", this.Active));

                id = (int)command.ExecuteScalar();
            }
            return id;
        }
        public virtual void UpdateUser()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update users set name=@name,lastname=@lastname,typeofuser=@typeofuser,password=@password, email=@email,  where username=@username";

                command.Parameters.Add(new SqlParameter("name", this.Name));
                command.Parameters.Add(new SqlParameter("lastname", this.LastName));
                command.Parameters.Add(new SqlParameter("typeofuser", this.TypeOfUser.ToString()));
                command.Parameters.Add(new SqlParameter("username", this.UserName));
                command.Parameters.Add(new SqlParameter("password", this.Password));
                command.Parameters.Add(new SqlParameter("email", this.Email));



                command.ExecuteNonQuery();
            }
        }
     
        public virtual void DeleteUser()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
         
                command.CommandText = @"delete from users  where username=@username and active=@active";

                command.Parameters.Add(new SqlParameter("active", this.Active));
                command.Parameters.Add(new SqlParameter("username", this.UserName));

                command.ExecuteNonQuery();
            }
        }
        public virtual int SelectedUserId()
        {
            int id = 1;
            using(SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select id from users where username like @Username";
                command.Parameters.Add(new SqlParameter("Username", this.UserName));

                id = (int)command.ExecuteScalar();
            }
            return id;
        }



    }
}


