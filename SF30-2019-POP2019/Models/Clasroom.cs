﻿using SF30_2019_POP2019.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF30_2019_POP2019.Models
{
    public class Clasroom : INotifyPropertyChanged
    {
        private int id;
        private int number;
        private string seats;
        private ETypeofClasroom type;
        private string institution;

        public Clasroom(int id, int number, string seats,string institution)
        {
            this.id = id;
            this.number = number;
            this.seats = seats;
            this.institution = institution;
            this._active = true;
        }
    

        public int Id
        {
            get { return id; }
            set
            {
                if (value.Equals(string.Empty))
                    throw new ArgumentException("This field is required");
                id = value;
            }
        }
        public int Number
        {
            get { return number; }
            set
            {
                if (value.Equals(string.Empty))
                    throw new ArgumentException("This field is required");
                number = value; OnPropertyChanged("Number");
            }
        }
        public string Seats
        {
            get { return seats; }
            set
            {
                seats = value; OnPropertyChanged("Seats");
               

            }
        }
        public ETypeofClasroom Type
        {
            get { return type; }
            set { type = value; OnPropertyChanged("Type"); }
        }
        public string Institution
        {
            get { return institution; }
            set { institution = value; OnPropertyChanged("Institution"); }
        }
        private bool _active;

        public bool Active
        {
            get { return _active; }
            set { _active = value; OnPropertyChanged("Active"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public Clasroom()
        {
            this.Seats = "";
            this._active = true;
        }

        public virtual Clasroom Clone()
        {
            return null;
        }
        public virtual int SaveClassroom()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into classrooms (number,seats,type,institution,active)output inserted.id values (@Number,@Seats,@Type,@Institution,@Active)";

                command.Parameters.Add(new SqlParameter("Number", this.Number));
                command.Parameters.Add(new SqlParameter("Seats", this.Seats));
                command.Parameters.Add(new SqlParameter("Type", this.Type.ToString()));
                command.Parameters.Add(new SqlParameter("Institution", this.Institution));
            



                command.Parameters.Add(new SqlParameter("Active", this.Active));

                id = (int)command.ExecuteScalar();
            }
            return id;
        }
        public virtual void UpdateClassroom()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"update classrooms set number=@name,type=@type,institution=@institution  where seats=@seats";

                command.Parameters.Add(new SqlParameter("number", this.Number));
                command.Parameters.Add(new SqlParameter("seats", this.Seats));
                command.Parameters.Add(new SqlParameter("type", this.Type.ToString()));
                command.Parameters.Add(new SqlParameter("institution", this.Institution));
           



                command.ExecuteNonQuery();
            }
        }

        public virtual void DeleteClassroom()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"delete from classrooms  where seats=@seats and active=@active";

                command.Parameters.Add(new SqlParameter("active", this.Active));
                command.Parameters.Add(new SqlParameter("seats", this.Seats));

                command.ExecuteNonQuery();
            }
        }
        public virtual int SelectedClassroomId()
        {
            int id = 1;
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"select id from classrooms where seats like @Seats";
                command.Parameters.Add(new SqlParameter("Seats", this.Seats));

                id = (int)command.ExecuteScalar();
            }
            return id;
        }



    }
}


    

