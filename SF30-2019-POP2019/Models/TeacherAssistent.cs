﻿using SF30_2019_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF30_2019_POP2019.Models
{
   public class TeacherAssistent : User
    {
        private User _profesor;

        public User Profesor
        {
            get { return _profesor; }
            set { _profesor = value; }
        }

        public TeacherAssistent(int id,string name, string username, string lastname, string password, string email) : base(id,name, username, lastname, password, email)
        {
            TypeOfUser = ETypeofUser.TA;
            
        }
        public override User Clone()
        {
            TeacherAssistent assistent = new TeacherAssistent(this.Id,this.Name, this.UserName,  this.LastName, this.Password, this.Email);
            return assistent;
        }
        public override int SaveUser()
        {
            int id = base.SaveUser();
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"insert into teacherassistants (id, professor_id) values (@id, @professor_id)";
                command.Parameters.Add(new SqlParameter("id", id));
                command.ExecuteNonQuery();

                int professor_id = Profesor.SelectedUserId();
                command.Parameters.Add(new SqlParameter("professor_id", professor_id));
                command.ExecuteNonQuery();
            }
            return id;
        }

    }
}

    
