﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF30_2019_POP2019
{
    /// <summary>
    /// Interaction logic for WindowForUnregistered.xaml
    /// </summary>
    public partial class WindowForUnregistered : Window
    {
        public WindowForUnregistered()
        {
            InitializeComponent();
            
        }

        private void PrikazProfesora_Click(object sender, RoutedEventArgs e)
        {
            Window1 fr5 = new Window1();
            fr5.Show();
            this.Hide();
        }

        private void PrikazInstitucija_Click(object sender, RoutedEventArgs e)
        {
            InstitutionRewiew fr6 = new InstitutionRewiew();
            fr6.Show();
            this.Hide();
        }

        private void PrikazAsistenata_Click(object sender, RoutedEventArgs e)
        {
            TARewiew fr7 = new TARewiew();
            fr7.Show();
            this.Hide();
        }
    }
}
