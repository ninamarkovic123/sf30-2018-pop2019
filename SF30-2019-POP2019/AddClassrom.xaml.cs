﻿using SF30_2019_POP2019.Models;
using SF30_2019_POP2019.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF30_2019_POP2019
{
    /// <summary>
    /// Interaction logic for AddClassrom.xaml
    /// </summary>
    public partial class AddClassrom : Window
    {
        public enum Status { ADD, EDIT }
        public Status _status;
        Clasroom clasroom;
        private Clasroom selectedClassroom;
        public AddClassrom(Clasroom clasroom, Status status = Status.ADD)
        {
            InitializeComponent();
            fillCombo();
            this.selectedClassroom = clasroom;
            cmbClassroomType.ItemsSource = new List<ETypeofClasroom>() { ETypeofClasroom.COMPUTER, ETypeofClasroom.WITHOUTCOMPUTER };

            if (clasroom.Seats.Equals(""))
            {
                _status = Status.ADD;
            }
            else
            {
                _status = Status.EDIT;

            }
            this.DataContext = clasroom;
        }

        void fillCombo()
        {
            using (SqlConnection conn = new SqlConnection(Data.CONNECTION_STRING))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();

                command.CommandText = @"select * from institutions";

                SqlDataReader dr = command.ExecuteReader();
                while (dr.Read()) {
                    string Name = dr.GetString(1);
                    cmbInstitution.Items.Add(Name);
                }

                

                //command.ExecuteNonQuery();
            }
        }
       private void Ok_Click(object sender, RoutedEventArgs e)
        {
            if (_status.Equals(Status.ADD))
            {
                if (ProveraClassroom(txtSeats.Text))
                {
                    MessageBox.Show($"Classroom with seats{txtSeats.Text} already exist!", "Error", MessageBoxButton.OK);
                    return;
                }
                else
                {

                    Data.Classrooms.Add(selectedClassroom);
                    selectedClassroom.SaveClassroom();
                    
                }
                if (_status.Equals(Status.EDIT))
                                   
                        selectedClassroom.UpdateClassroom();
            }
            this.DialogResult = true;
            this.Close();
        }
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private bool ProveraClassroom(string seats)
        {
            foreach (Clasroom clasroom in Data.Classrooms)
            {
                if (clasroom.Seats.Equals(seats))
                {
                    return true;
                }
            }
            return false;
        }
        private Clasroom GetClassroom(string seats)
        {
            foreach (Clasroom clasroom in Data.Classrooms)
            {
                if (clasroom.Seats.Equals(seats))
                {
                    return clasroom;
                }
            }
            return null;
        }
    }
}