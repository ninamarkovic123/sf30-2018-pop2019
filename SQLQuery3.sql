﻿CREATE TABLE [dbo].[Institutions] (
    [Id]         INT          IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (50) NOT NULL,
	[Location]   VARCHAR (50) NOT NULL,
	[Active]     VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);
