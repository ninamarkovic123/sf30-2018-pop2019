﻿
CREATE TABLE [dbo].[Users] (
    [Id]         INT          IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (50) NOT NULL,
    [Username]   VARCHAR (50) NOT NULL,
    [Email ]     VARCHAR (50) NOT NULL,
    [TypeOfUser] VARCHAR (50) NOT NULL,
    [Active]     VARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);
CREATE TABLE [dbo].[Administrators] (
    [Id] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [fk_users_administrators] FOREIGN KEY ([Id]) REFERENCES [dbo].[Users] ([Id])
);
CREATE TABLE [dbo].[Professors] (
    [Id] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [fk_users_professors] FOREIGN KEY ([Id]) REFERENCES [dbo].[Users] ([Id])
);
CREATE TABLE [dbo].[TeacherAssistants] (
    [Id]           INT NOT NULL,
    [Professor_Id] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [fk_users_tas] FOREIGN KEY ([Id]) REFERENCES [dbo].[Users] ([Id]),
    CONSTRAINT [fk_professors_tas] FOREIGN KEY ([Professor_Id]) REFERENCES [dbo].[Professors] ([Id])
);